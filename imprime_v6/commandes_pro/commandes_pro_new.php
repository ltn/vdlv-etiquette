<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Vincent dans les vapes - Commandes Pro</title>
<link href="../styles/gestion.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../styles/jquery-ui-1.7.2.custom.css" />
<link type="text/css" rel="stylesheet" href="../styles/superfish.css" />
<link type="text/css" rel="stylesheet" href="../styles/autocomplete.css" />
<link type="text/css" rel="stylesheet" href="../styles/menus_fieldset.css" media="all" />

<script type="text/javascript" src='../scripts/ajax.js'></script>
<script type="text/javascript" src='../scripts/biblio.js'></script>
<script type="text/javascript" src="../scripts/jquery1.8.2.js"></script>
<script type="text/javascript" src="../scripts/autoComplete/autoCompleteur.js"></script>

<script type="text/javascript" src="../scripts/superfish.js"></script>

<script>

var $mn = jQuery.noConflict();
$mn(function() {	
	
	// initialise examples
	$mn('#sample-menu-1').superfish({
		animation: {height:'show'},
		cssArrows:     false,
		speed:         'fast',
		speedOut:      'fast',
		delay: 100
	});

});

</script>

<script type=text/javascript>
	
	var $tt = jQuery.noConflict();
   
   $tt(document).ready(function()
   {
	
   //Ajout d'une quantite sur les classiques
    $tt('input.q_add_classique').keyup(function() {
      var IDA=$tt(this).parent().parent().attr('id');
      var sum = 0;
      var sum_col = 0;
      var sum_ligne = 0;
      var sum_total = 0;
		
		// Calcul de la somme d une ligne
      $tt('.'+IDA).each(function() {
          if($tt(this).val()!='') {
			 	sum += parseInt($tt(this).val());
				$tt(this).addClass('bg_on');
			 } else {
				$tt(this).removeClass('bg_on');
			 }
		});
		// Affichage de la somme d une ligne
      $tt('#sum_'+IDA).html(sum);
		
		// Calcul des sommes des lignes
	<? foreach($data_out['liste_saveurs'] as $id_saveur => $saveur) { ?>
		  $tt('.ligne_<?=$id_saveur?>').each(function() {
			  if($tt(this).val()!='') {
					sum_ligne += parseInt($tt(this).val());
				 }
			});
			// Affichage de la somme de la ligne
		  $tt('#sum_ligne<?=$id_saveur?>').html(sum_ligne);
			sum_ligne = 0;
	<? } ?>
		
   });   

   });
</script>

<script language="JavaScript" type="text/JavaScript">
function isDate(d) {
	// Cette fonction permet de v�rifier la validit� d'une date au format jj/mm/aaaa
	if (d == "") // si la variable est vide on retourne faux
		return false;
	
	e = new RegExp("^[0-9]{2}\/[0-9]{2}\/([0-9]{2}|[0-9]{4})$");

	if (!e.test(d)) // On teste l'expression r�guli�re pour valider la forme de la date
		return false; // Si pas bon, retourne faux

	// On s�pare la date en 3 variables pour v�rification, parseInt() converti du texte en entier
	j = parseInt(d.split("/")[0], 10); // jour
	m = parseInt(d.split("/")[1], 10); // mois
	a = parseInt(d.split("/")[2], 10); // ann�e

	// D�finition du dernier jour de f�vrier
	// Ann�e bissextile si annn�e divisible par 4 et que ce n'est pas un si�cle, ou bien si divisible par 400
	if (a%4 == 0 && a%100 !=0 || a%400 == 0) fev = 29;
	else fev = 28;

	// Nombre de jours pour chaque mois
	nbJours = new Array(31,fev,31,30,31,30,31,31,30,31,30,31);

	// Enfin, retourne vrai si le jour est bien entre 1 et le bon nombre de jours, idem pour les mois, sinon retourn faux
	return ( m >= 1 && m <=12 && j >= 1 && j <= nbJours[m-1] );
}

function check_form(frm) {
	var msg = "";

	if (document.forms[frm].pays.value == '')   {
		msg += "Veuillez saisir une langue\n";
		document.getElementById('pays').style.backgroundColor = "#FAB9C2";
	}

	if (document.forms[frm].nom_client.value == '')   {
		msg += "Veuillez saisir un client\n";
		document.getElementById('nom_client').style.backgroundColor = "#FAB9C2";
	}

	if (document.forms[frm].id_pg_vg.value == '')   {
		msg += "Veuillez saisir un taux PG/VG\n";
		document.getElementById('id_pg_vg').style.backgroundColor = "#FAB9C2";
	}

	if (document.forms[frm].date_commande.value == '')   {
		msg += "Veuillez saisir une date pour la commande\n";
		document.getElementById('date_commande').style.backgroundColor = "#FAB9C2";
	}

	if (!((document.forms[frm].dluo.value == '') && (document.forms[frm].num_lot.value == '')) && !((document.forms[frm].dluo.value != '') && (document.forms[frm].num_lot.value != '')))   {
		msg += "Veuillez saisir une DLUO et un numéro de lot pour la commande\n";
		document.getElementById('dluo').style.backgroundColor = "#FAB9C2";
		document.getElementById('num_lot').style.backgroundColor = "#FAB9C2";
	}

	if (((document.forms[frm].dluo.value != '') && (document.forms[frm].num_lot.value != '')) && !isDate(document.forms[frm].dluo.value)) {
		msg += "Les dates doivent etre au format JJ/MM/YYYY et doivent etre valides\n";
		document.getElementById('dluo').style.backgroundColor = "#FAB9C2";
	}

	if (msg == "") return(true);
	else   {
		alert(msg);
		return(false);
	}
}
</script>

</head>

<body>
<div id="wrapper">
	<div id="header">
		<div id="logo"></div>
		<div id="titre">GESTION DES IMPRESSIONS</div>
		<div id="deconnexion"><a href="../dilasys/actions.php?action=Login_Quitter"><img src="../imgs/deconnexion.png"
		 alt="" border="0" align="absmiddle" /></a></div>
	</div>
   <div id="smoothmenu1" class="ddsmoothmenu">
   <? $menu = 'pro'; $smenu = 'pro_n'; include('../menu_haut.php'); ?>
   </div>

	<div id="main">
		<div id="principal">
		  <p>&nbsp;</p>
                 <h1>COMMANDES PRO - CREATION</h1>
                 <p>&nbsp;</p>
				<fieldset style="display:<?=(!isset($data_out['onglet']) || $data_out['onglet'] == 'classique') ? '' : 'none'?>; padding:10px;" id="fieldset_classique">
					<legend style="padding:0; margin-left: 20px;">
						<table style="border-collapse:collapse;" cellpadding="3">
							<tr>
							<? $nb=count($data_out['liste_gammes']); 
								for($i=0;$i<$nb;$i++) { 
									$gamme = array_shift($data_out['liste_gammes']); 
									$position = 'bas';
									if ($gamme['id_gamme'] == $data_out['id_gamme'] && $i == 0) $position = 'select_gauche';
									if ($gamme['id_gamme'] == $data_out['id_gamme'] && $i != 0 && $i != $nb-1) $position = 'select_milieu';
									if ($gamme['id_gamme'] == $data_out['id_gamme'] && $i == $nb-1) $position = 'select_droite';
							?>
							<td class="fieldset_<?=$position?>"><?=$data_out['id_gamme'] == $gamme['id_gamme'] ? '<strong>' : '['?><a href="actions.php?action=CommandesPro_New&id_gamme=<?=$gamme['id_gamme']?>"><?=$gamme['libelle']?></a><?=$data_out['id_gamme'] == $gamme['id_gamme'] ? '</strong>' : ']'?></td>
							<? } ?>
							</tr>
						</table>
					</legend>
				   <p align="center">&nbsp;</p>
					 <form method="post" action="actions.php" name="frm_commande_add" onsubmit="return check_form('frm_commande_add');" autocomplete="off">
					  <center>
					  <table width="1100" border="0" cellspacing="0" cellpadding="1">
						<tbody>
						  <tr id="saveur">
							<td width="80" align="right" valign="middle"><span class="oblig"><strong>Langue :</strong></span></td>
							<td width="100" align="center">
							<select name="pays" class="formulaire81b oblig">
							   <?php foreach($GLOBALS['LISTE_PAYS'] as $pays) { ?>
								<option value="<?=$pays['code']?>"><?=$pays['libelle']?></option>
							   <? } ?>
						   </select>
							</td>
							<td width="50" align="right"><span class="oblig"><strong>Taux :</strong></span></td>
							<td width="60" align="center">
							<select name="id_pg_vg" id="id_pg_vg" class="formulaire81b oblig">
							   <option value="">Pr&eacute;cisez</option>
						 <? foreach($data_out['liste_pg_vg'] as $id_pg_vg => $pg_vg) { ?>
							   <option value="<?=$id_pg_vg?>"><?=$pg_vg['name']?></option>
						 <? } ?>
							</select></td>
							<td width="60" align="center">&nbsp;</td>
							<td width="80" align="right">CLIENT :</td>
							<td width="60" align="left">
							   <input name="nom_client" type="text" class="formulaire81 form_bleu" id="nom_client" size="30" onchange="this.value=''; document.frm_commande_add.id_customer.value=0;"/>
							   <div class="ajax_class" id="liste_clients" style="text-align:left; background: #FFFFFF;"></div>
							   <script type="text/javascript">
							   new AutoCompleteur('nom_client', 'liste_clients', 'actions.php?action=AUTO_ListeClients_ADD&form=frm_commande_add', {Ajax: {paramName:'nom_client'}, Extra: {afterValidate: Function()}, Global: {nbrMaxItem: 30} });
							   </script>
							</td>
							<td width="60" align="center">&nbsp;</td>
							<td width="80" align="right">N&deg; LOT: </td>
							<td width="60" align="left"><input name="num_lot" type="text" class="formulaire81 form_bleu" 
							 id="num_lot" size="8" /></td>
							<td width="60" align="center">&nbsp;</td>
							<td width="60" align="right">DATE: </td>
							<td width="60" align="left"><input name="date_commande" type="text" class="formulaire81 form_bleu" 
							 id="date_commande" value="<?=gmdate("d/m/Y")?>" size="8" /></td>
							<td width="60" align="center">&nbsp;</td>
							<td width="60" align="right">DLUO: </td>
							<td width="60" align="left">
							<?php
								$dluo_tmsp = gmmktime(0,0,0,gmdate("m")+1,gmdate("d"),gmdate("Y")+1);
								$dluo = gmdate("d/m/Y", $dluo_tmsp);
							?>
							<input name="dluo" type="text" class="formulaire81 form_bleu" 
							 id="dluo" size="8"/></td>
							<td width="100" align="right" valign="middle"><span class="oblig"><strong>Co-marque :</strong></span></td>
							<td align="center">
							<select name="id_marque" id="id_marque" class="formulaire81b oblig">
							   <option value="">Pr&eacute;cisez</option>
							   <? foreach($data_out['liste_marques'] as $id_marque => $marque) { ?>
								<option value="<?=$id_marque?>"><?=$marque['libelle']?></option>
							   <? } ?>
							</select></td>
						  </tr>
						</tbody>
					  </table>
					  </center>
					  <div class="cl1"><img src="../imgs/point.gif" /></div>
					  <center>
					  <table width="840" border="0" cellspacing="0" cellpadding="1" class="gestion_ac ord zebra" align="center">
						<thead class="normal">
						  <tr>
							<th align="center"><strong>SAVEURS</strong></th>
							<th colspan="<?=count($data_out['liste_taux_nicotine'])?>" align="center"><strong>TAUX DE NICOTINE</strong></th>
							<th width="85" align="center" class="td_somme">Total</th>
						  </tr>
						  <tr>
							<th align="center">&nbsp;</th>
					 <? foreach($data_out['liste_taux_nicotine'] as $id_nico => $nico) { ?>
							<th width="60" align="center"><?=$nico['name']?> mg</th>
					  <? } ?>
							<th align="center" class="td_somme">&nbsp;</th>
						  </tr>
						</thead>
						<tbody>
						<? $nligne=0; ?>
				   <? foreach($data_out['liste_saveurs'] as $id_saveur => $saveur) { ?>
				   <? $nligne++;
						if($nligne%15==0){ ?>
							 <tr>
							<th align="center">&nbsp;</th>
								 <? foreach($data_out['liste_taux_nicotine'] as $id_nico => $nico) { ?>
										<th width="60" align="center"><?=$nico['name']?> mg</th>
								  <? } ?>
							<th align="center" class="td_somme">&nbsp;</th>
						  </tr>
						<? } ?>
						
						  <tr id="saveur<?=$id_saveur?>">
							<td align="center"><?=$saveur['name']?></td>
					 <? foreach($data_out['liste_taux_nicotine'] as $id_nico => $nico) { ?>
							<td align="center"><input name="saveur_<?=$id_saveur?>_taux_<?=$nico['id_attribute']?>" type="text"
							 class="formulaire81 saveur<?=$id_saveur?> q_add_classique align_r ligne_<?=$id_saveur?> col_<?=$nico['id_attribute']?><? $col++; ?>" 
							 id="saveur<?=$saveur['name']?>_taux_<?=$nico['id_attribute']?>" size="3" /></td>
					  <? } ?>
							<td align="center" class="td_somme"><span id="sum_ligne<?=$id_saveur?>" class="sum_ligne"></span>&nbsp;</td>
						  </tr>
				   <? } ?>
						</tbody>
					  </table>
					  </center>
					  <p align="center">&nbsp;</p>
					  <input type="hidden" name="action" value="CommandesPro_ADD"/>
					  <input type="hidden" name="id_customer"/>
					  <input type="hidden" name="id_gamme" value="<?=$data_out['id_gamme']?>" />
					  <p align="center"><input type="submit" name="button" id="button" value="Valider" /></p>
					 </form>
				</fieldset>	 

               <p align="center">&nbsp;</p></td>
	  </div>
	</div>
</div>
</body>
</html>
