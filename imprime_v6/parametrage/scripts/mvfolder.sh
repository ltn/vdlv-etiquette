#!/bin/sh

prodFolderPath=/var/www/boutique/www/imprime_v6/modeles/
devFolderPath=/var/www/dev/www/imprime_v6/modeles/

datetime=$(date +%Y%m%d%H%M%S)
echo "$datetime"

## on calcule le md5 du dossier img de prod
echo "md5 for prod"
cd "$prodFolderPath/img"
md5=$(find -type f -exec md5sum '{}' \;)
md5prod=$(echo -n $md5 | md5sum)
echo $md5prod

## on calcule le md5 du dossier img de dev
echo "md5 for dev"
cd "$devFolderPath/img"
md5=$(find -type f -exec md5sum '{}' \;)
md5dev=$(echo -n $md5 | md5sum)
echo $md5dev

## si les deux md5 sont égaux, on ne fait rien
if [ "$md5prod" = "$md5dev" ]; then
   echo "equal"
# si les deux md5 sont différents
else
   echo "not equal"
   echo "prod img mv to img_sav_$datetime"
## on déplace le dossier img de prod vers img_sav_datetime
   mv "$prodFolderPath/img" "$prodFolderPath/img_sav_$datetime"
   echo "dev img cp to prod img"
## on copie le dossier img de dev vers le dossier img de prod
   cp -r "$devFolderPath/img" "$prodFolderPath/img"
   chmod 777 -R "$prodFolderPath/img"
## ensuite on ne garde que les 5 derniers sauvegardes:
   echo "keep only last 5 sav versions"
   cd $prodFolderPath
## 1. on commence par s'assurer que la dernière sauvegarde à bien une date de création de fichier la plus récente  
   touch newfile
   touch -r newfile "$prodFolderPath/img_sav_$datetime"
   rm newfile
## 2. on filtre tous les fichier en img_sav_* et on ne garde que les 5 plus récents   
   ls -dtp img_sav_* | tail -n +6 | xargs -d '\n' -r rm -rf --
fi
