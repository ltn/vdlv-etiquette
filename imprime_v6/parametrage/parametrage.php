7<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Param&eacute;trage - Vincent dans les vapes</title>
<link href="../styles/gestion.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../styles/jquery-ui-1.7.2.custom.css" />
<link rel="stylesheet" type="text/css" href="../styles/superfish.css" />
<link type="text/css" rel="stylesheet" href="../styles/autocomplete.css" />
<link type="text/css" rel="stylesheet" href="../styles/menus_fieldset.css" media="all" />

<script type="text/javascript" src='../scripts/ajax.js'></script>
<script type="text/javascript" src='../scripts/drag.js'></script>
<script type="text/javascript" src='../scripts/biblio.js'></script>
<script type="text/javascript" src='../scripts/show-hide.js'></script>
<script type="text/javascript" src='../scripts/app-disp_gris.js'></script>
<script type="text/javascript" src="../scripts/jquery1.8.2.js"></script>
<script type="text/javascript" src="../scripts/autoComplete/autoCompleteur.js"></script>
<script type="text/javascript" src="../scripts/scrollTop.js"></script>

<script type="text/javascript" src="../scripts/superfish.js"></script>

<script type="text/javascript">
function popup_pays_upd(id_pays) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=POPUP_Pays_UPD';
	url += '&id_pays='+id_pays;
	document.getElementById('div_pays_upd').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_pays_upd', true);
}

function popup_marque_upd(id_marque) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=POPUP_Marque_UPD';
	url += '&id_marque='+id_marque;
	document.getElementById('div_marque_upd').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_marque_upd', true);
}

function popup_gamme_upd(id_gamme) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=POPUP_Gamme_UPD';
	url += '&id_gamme='+id_gamme;
	document.getElementById('div_gamme_upd').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_gamme_upd', true);
}

function affiche_gamme(id_gamme, pays) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=AJAX_bloc_gamme';
	url += '&id_gamme='+id_gamme;
	url += '&pays='+pays;
	document.getElementById('div_gamme').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_gamme', true);
}

function affiche_fonds(id_gamme, pays) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=AJAX_affiche_fonds';
	url += '&id_gamme='+id_gamme;
	url += '&pays='+pays;
	document.getElementById('div_affiche_fonds').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_affiche_fonds', true);
}

function affiche_styles(id_gamme, pays) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=AJAX_affiche_styles';
	url += '&id_gamme='+id_gamme;
	url += '&pays='+pays;
	document.getElementById('div_affiche_styles').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_affiche_styles', true);
}
function affiche_dgccrf(id_gamme, pays) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=AJAX_affiche_dgccrf';
	url += '&id_gamme='+id_gamme;
	url += '&pays='+pays;
	document.getElementById('div_affiche_dgccrf').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_affiche_dgccrf', true);
}

function affiche_textes(pays) {
	var httpRequest = getXMLHTTP();
	var url = 'actions.php?action=AJAX_affiche_textes';
	url += '&pays='+pays;
	document.getElementById('div_affiche_textes').innerHTML = '';
	envoi_AJAX(httpRequest, url, 'div_affiche_textes', true);
}

function affiche_composition() {
	var id_gamme = document.frm_test.id_gamme.value;
	var saveur = document.frm_test.saveur.value;
	var id_pg_vg = document.frm_test.id_pg_vg.value;
	var id_taux_nicotine = document.frm_test.id_taux_nicotine.value;
	var pays = document.frm_test.pays.value;

	var httpRequest = getXMLHTTP();
	var url = 'actions.php';
	var params = 'action=Affiche_Composition';
	params += '&id_gamme='+id_gamme;
	params += '&saveur='+saveur;
	params += '&id_pg_vg='+id_pg_vg;
	params += '&id_taux_nicotine='+id_taux_nicotine;
	params += '&pays='+pays;
	envoi_AJAX_POST(httpRequest, url, params, 'div_affiche_composition', true);
}

</script>

<script>
var $mn = jQuery.noConflict();
$mn(function() {	
	
	// initialise examples
	$mn('#sample-menu-1').superfish({
		animation: {height:'show'},
		cssArrows:     false,
		speed:         'fast',
		speedOut:      'fast',
		delay: 100
	});

});

</script>

</head>

<body onload="affiche_gamme(<?=$data_out['id_gamme']?>, '<?=$data_out['pays']?>')">
<div id="wrapper">
	<div id="header">
		<div id="logo"></div>
		<div id="titre">PARAMETRAGE</div>
		<div id="deconnexion"><a href="../dilasys/actions.php?action=Login_Quitter"><img src="../imgs/deconnexion.png"
		 alt="D&eacute;connexion" border="0" align="absmiddle" /></a></div>
	</div>
   <div id="smoothmenu1" class="ddsmoothmenu">
   <? $menu = 'param'; $smenu = 'param'; include('../menu_haut.php'); ?>
   </div>

	<div id="main">
		<div id="principal">
		  <p>&nbsp;</p>

						   <fieldset style="background:#EEEEEE">
						   <legend style="background:#FFFFFF"><strong>CODES EAN :</strong></legend><br />
							  <form autocomplete="off" method="post" name="frm_ean" action="actions.php">
									Extraire les codes EAN cr&eacute;&eacute;s : <input type="hidden" value="EAN_Export_XL" name="action">
									  <input type="submit" value="Extraire" ></td>
								 </form>
							<br /></fieldset>

<span style="display: inline-block; margin: 1em;  background: red; padding: 0.5em; color: white; font-weight: bold;">> <a href="#" style="color: white" onclick="deploy_to_prod(event)">Déployer la configuration en production</a> <span id="deploy_state"></span></span>
<span style="float: right; display: inline-block; margin: 1em;  background: red; padding: 0.5em; color: white; font-weight: bold;">> <a href="#" style="color: white" onclick="copy_to_prod(event)">Copier les images en production</a> <span id="copy_state"></span></span>

						   <fieldset style="background:#EEEEEE">
						   <legend onclick="showhide('div_gestion_langues')"><strong>GESTION DES LANGUES :</strong>&nbsp;<img src="../imgs/view.gif" border="0" />&nbsp;</legend><br />
			<div id="div_gestion_langues" style="display:<?=(!empty($data_srv['show_hide']) && $data_srv['show_hide'] == 'div_gestion_langues') ? '' : 'none'?>">
			 
						  <!-- <legend style="background:#FFFFFF"><strong>GESTION DES LANGUES :</strong></legend><br />-->
                        <table width="400" border="0" cellspacing="0" cellpadding="1" class="gestion_ac ord zebra">
						<thead class="normal">
							<tr>
                              <th align="center"><strong>LIBELLE</strong></th>
                              <th align="center"><strong>CODE ISO</strong></th>
                              <th align="center"><a href="javascript: void(0)" onclick="App('Pays_ADD_WD')" class="scrolltop" name="Pays_ADD">Ajouter</a></th>
							 </tr>
						</thead>
                          <tbody>
                            <? foreach($GLOBALS['LISTE_PAYS'] as $pays) { ?>
                            <tr>
                              <td align="center"><?=$pays['libelle']?></td>
                              <td align="center"><?=$pays['code']?></td>
                              <td align="center"><a href="javascript: void(0)" onclick="App('Pays_UPD_WD'); popup_pays_upd(<?=$pays['id_pays']?>)"
                               class="scrolltop" name="Pays_UPD">Modifier</a></td>
                            </tr>
                            <? } ?>
                          </tbody>
                        </table>						   
							<br /></div></fieldset>
							  <p>&nbsp;</p>
							  <p>&nbsp;</p>
						   <p align="center">&nbsp;</p>

						   <fieldset style="background:#EEEEEE">
						    <legend onclick="showhide('div_gestion_comarquage')"><strong>GESTION DES CO-MARQUAGE :</strong>&nbsp;<img src="../imgs/view.gif" border="0" />&nbsp;</legend><br />
			<div id="div_gestion_comarquage" style="display:<?=(!empty($data_srv['show_hide']) && $data_srv['show_hide'] == 'div_gestion_comarquage') ? '' : 'none'?>">
			 
						 <!--  <legend style="background:#FFFFFF"><strong>CO-MARQUAGE :</strong></legend><br />-->
                        <table width="400" border="0" cellspacing="0" cellpadding="1" class="gestion_ac ord zebra">
						<thead class="normal">
							<tr>
                              <th align="center"><strong>CO-MARQUES</strong></th>
                              <th align="center"><a href="javascript: void(0)" onclick="App('Marque_ADD_WD')" class="scrolltop" name="Marque_ADD">Ajouter</a></th>
							 </tr>
						</thead>
                          <tbody>
                            <? foreach($data_out['liste_marques'] as $marque) { ?>
                            <tr>
                              <td align="center"><?=$marque['libelle']?></td>
                              <td align="center"><a href="javascript: void(0)" onclick="App('Marque_UPD_WD'); popup_marque_upd(<?=$marque['id_marque']?>)"
                               class="scrolltop" name="Marque_UPD">Modifier</a></td>
                            </tr>
                            <? } ?>
                          </tbody>
                        </table>						   
							<br /></div></fieldset>
							  <p>&nbsp;</p>
							  <p>&nbsp;</p>
						   <p align="center">&nbsp;</p>

						   <fieldset style="background:#EEEEEE">
						     <legend onclick="showhide('div_gestion_gammes')"><strong>GESTION DES GAMMES :</strong>&nbsp;<img src="../imgs/view.gif" border="0" />&nbsp;</legend><br />
			<div id="div_gestion_gammes" style="display:<?=(!empty($data_srv['show_hide']) && $data_srv['show_hide'] == 'div_gestion_gammes') ? '' : 'none'?>">
			 
						   <!--<legend style="background:#FFFFFF"><strong>GAMMES :</strong></legend><br />-->
                        <table width="600" border="0" cellspacing="0" cellpadding="1" class="gestion_ac ord zebra">
						<thead class="normal">
							<tr>
							  <th align="center"><strong>N&deg;</strong></th>
                              <th align="center"><strong>NOM GAMME</strong></th>
                              <th align="center"><strong>NOM GAMME</strong></th>
                              <th align="center"><strong>MARQUE ASSOCIEE</strong></th>
                              <th align="center"><strong>CONTENANCE</strong></th>
                              <th align="center"><strong>GAMME DEFAUT</strong></th>
                             
                              <th align="center"><a href="javascript: void(0)" onclick="App('Gamme_ADD_WD')" class="scrolltop" name="Gamme_ADD">Ajouter</a></th>
							 </tr>
						</thead>
                          <tbody>
                            <? foreach($data_out['liste_gammes'] as $gamme) { ?>
                            <tr>
								<td align="center"><?=$gamme['id_gamme']?></td>
                              <td align="center"><?=$gamme['libelle']?></td>
                              <td align="center"><?=$data_out['liste_attribute_groups'][$gamme['id_attribute_group']]['name']?></td>
                              <td align="center"><?=$gamme['marque_associee']?></td>
                              <td align="center"><?=$gamme['contenance_associee']?></td>
                              <td align="center"><?=$gamme['gamme_defaut'] ? 'oui' : ''?></td>                             
                              <td align="center"><a href="javascript: void(<?=$gamme['id_gamme']?>)" onclick="App('Gamme_UPD_WD'); popup_gamme_upd(<?=$gamme['id_gamme']?>)"
                               class="scrolltop" name="Gamme_UPD">Modifier</a></td>
                            </tr>
                            <? } ?>
                          </tbody>
                        </table>						   
							<br /></div></fieldset>
							  <p>&nbsp;</p>
							  <p>&nbsp;</p>
						   <p align="center">&nbsp;</p>

							<div id="div_gamme"></div>
							 
						   <p align="center">&nbsp;</p>
                           
                           <div id="div_gamme_design"></div>
							 
							<p>&nbsp;</p>
						   <fieldset style="background:#EEEEEE">
						   <legend onclick="showhide('div_gestion_textes')"><strong>TEXTES DE L'ETIQUETTE</strong>&nbsp;<img src="../imgs/view.gif" border="0" />&nbsp;</legend><br />
							<div id="div_gestion_textes" style="display:<?=(!empty($data_srv['show_hide']) && $data_srv['show_hide'] == 'div_gestion_textes') ? '' : 'none'?>">
							   <div id="div_affiche_textes"><img src="../imgs/point.gif" onload="affiche_textes('<?=!empty($data_out['pays']) ? $data_out['pays'] : 'fr'?>')" /></div>
						   </div>
						   </fieldset>
							  <p>&nbsp;</p>
							  <p>&nbsp;</p>
						   <p align="center">&nbsp;</p>

			<div class="cl1"><img src="../imgs/point.gif" /></div>
		  <p>&nbsp;</p>
	  </div>
	</div>
</div>
</body>

<div id="fd_gris"></div>

<div id="ID_MENUMarque_ADD_WD" style="
		position:absolute;
		width:600px;
		z-index:11;
		padding:2px 2px 10px;
      border: solid #a5937b 1px;
      background: #fff;
      border-radius: 6px 6px 0 0;
		display: none;">
		<div id="ID_MENUMarque_ADD">	
			<div class="pop_title">
            	<div class="pop_close">
					<a onClick="Disp('Marque_ADD_WD');"><img
					 src="../imgs/ico_close.png" alt="Fermer" title="Fermer" border="0"></a>
				</div>
				<div id="div_titre_prestation_upd">Ajouter une co-marque</div>
			</div>
		</div>
		<form name="frm_marque_add" method="post" action="actions.php" autocomplete="off" enctype="multipart/form-data">
      <center>
			<table width="400" class="tab_memo" class="dialog_content" align="center">
				<tr>
				  <td align="left" valign="top"><strong>Libell&eacute; co-marque :</strong></td>
				  <td align="left" valign="top"><input type="text" name="libelle" value="<?=$data_out['libelle']?>" class="formulaire81"  /></td>
			  </tr>
		  </table>
		  <br />
			 <input type="submit" name="Submit" value="Ajouter"/>
			 <input type="hidden" name="timestamp" value="<?=time()?>" />
			 <input type="hidden" name="action" value="Marque_ADD" />
        </center>
	  </form>
</div>

<div id="ID_MENUMarque_UPD_WD" style="
		position:absolute;
		width:600px;
		z-index:11;
		padding:2px;
      border: solid #a5937b 1px;
      background: #fff;
      border-radius: 6px 6px 0 0;
		display: none;">
		<div id="ID_MENUMarque_UPD">	
			<div class="pop_title">
            	<div class="pop_close">
					<a onClick="Disp('Marque_UPD_WD');"><img
					 src="../imgs/ico_close.png" alt="Fermer" title="Fermer" border="0"></a>
				</div>
				<div id="div_titre_prestation_upd">Modification de la co-marque</div>
			</div>
		</div>
		<div id='div_marque_upd'></div>
</div>

<div id="ID_MENUPays_ADD_WD" style="
		position:absolute;
		width:600px;
		z-index:11;
		padding:2px 2px 10px;
      border: solid #a5937b 1px;
      background: #fff;
      border-radius: 6px 6px 0 0;
		display: none;">
		<div id="ID_MENUPays_ADD">	
			<div class="pop_title">
            	<div class="pop_close">
					<a onClick="Disp('Pays_ADD_WD');"><img
					 src="../imgs/ico_close.png" alt="Fermer" title="Fermer" border="0"></a>
				</div>
				<div id="div_titre_prestation_upd">Ajouter un pays</div>
			</div>
		</div>
		<form name="frm_pays_add" method="post" action="actions.php" autocomplete="off" enctype="multipart/form-data">
      <center>
			<table width="400" class="tab_memo" class="dialog_content" align="center">
				<tr>
				  <td align="left" valign="top"><strong>Libell&eacute; :</strong></td>
				  <td align="left" valign="top"><input type="text" name="libelle" class="formulaire81"  /></td>
			  </tr>
				<tr>
				  <td align="left" valign="top"><strong>Code ISO :</strong></td>
				  <td align="left" valign="top"><input type="text" name="code" class="formulaire81"  /></td>
			  </tr>
		  </table>
		  <br />
			 <input type="submit" name="Submit" value="Ajouter"/>
			 <input type="hidden" name="timestamp" value="<?=time()?>" />
			 <input type="hidden" name="action" value="Pays_ADD" />
        </center>
	  </form>
</div>

<div id="ID_MENUPays_UPD_WD" style="
		position:absolute;
		width:600px;
		z-index:11;
		padding:2px;
      border: solid #a5937b 1px;
      background: #fff;
      border-radius: 6px 6px 0 0;
		display: none;">
		<div id="ID_MENUPays_UPD">	
			<div class="pop_title">
            	<div class="pop_close">
					<a onClick="Disp('Pays_UPD_WD');"><img
					 src="../imgs/ico_close.png" alt="Fermer" title="Fermer" border="0"></a>
				</div>
				<div id="div_titre_pays_upd">Modification du pays</div>
			</div>
		</div>
		<div id='div_pays_upd'></div>
</div>

<div id="ID_MENUGamme_ADD_WD" style="
		position:absolute;
		width:600px;
		z-index:11;
		padding:2px 2px 10px;
      border: solid #a5937b 1px;
      background: #fff;
      border-radius: 6px 6px 0 0;
		display: none;">
		<div id="ID_MENUGamme_ADD">	
			<div class="pop_title">
            	<div class="pop_close">
					<a onClick="Disp('Gamme_ADD_WD');"><img
					 src="../imgs/ico_close.png" alt="Fermer" title="Fermer" border="0"></a>
				</div>
				<div id="div_titre_prestation_upd">Ajouter une gamme</div>
			</div>
		</div>
		<form name="frm_gamme_add" method="post" action="actions.php" autocomplete="off" enctype="multipart/form-data">
      <center>
			<table width="400" class="tab_memo" class="dialog_content" align="center">
				<tr>
				  <td align="left" valign="top"><strong>Libell&eacute; gamme :</strong></td>
				  <td align="left" valign="top"><input type="text" name="libelle" value="<?=$data_out['libelle']?>" class="formulaire81"  /></td>
			  </tr>
				<tr>
				  <td align="left" valign="top"><strong>Groupe d'attribut PS :</strong></td>
				  <td align="left" valign="top">
				   <select name="id_attribute_group" class="formulaire81b oblig">
				   	<? foreach($data_out['liste_attribute_groups'] as $group) { ?>
				   		<option value="<?=$group['id_attribute_group']?>"><?=$group['name']?></option>
					<? } ?>
					</select>
				  </td>
				<tr>
				  <td align="left" valign="top"><strong>Marque associ&eacute;e :</strong></td>
				  <td align="left" valign="top"><input type="text" name="marque_associee" value="<?=$data_out['marque_associee']?>" class="formulaire81"  /></td>
			  </tr>
				<tr>
				  <td align="left" valign="top"><strong>Contenance associ&eacute;e :</strong></td>
				  <td align="left" valign="top"><input type="text" name="contenance_associee" value="<?=$data_out['contenance_associee']?>" class="formulaire81"  /></td>
			  </tr>
				<tr>
				  <td align="left" valign="top"><strong>Gamme par d&eacute;faut (pour les particuliers) :</strong></td>
				  <td align="left" valign="top">
				  	<input type="hidden" name="gamme_defaut" value="0" />
				  	<input type="checkbox" name="gamme_defaut" value="1" class="formulaire81"  />
					</td>
			  </tr>
			  </tr>
		  </table>
		  <br />
			 <input type="submit" name="Submit" value="Ajouter"/>
			 <input type="hidden" name="timestamp" value="<?=time()?>" />
			 <input type="hidden" name="action" value="Gamme_ADD" />
        </center>
	  </form>
</div>

<div id="ID_MENUGamme_UPD_WD" style="
		position:absolute;
		width:600px;
		z-index:11;
		padding:2px;
      border: solid #a5937b 1px;
      background: #fff;
      border-radius: 6px 6px 0 0;
		display: none;">
		<div id="ID_MENUGamme_UPD">	
			<div class="pop_title">
            	<div class="pop_close">
					<a onClick="Disp('Gamme_UPD_WD');"><img
					 src="../imgs/ico_close.png" alt="Fermer" title="Fermer" border="0"></a>
				</div>
				<div id="div_titre_gamme_upd">Modification de la gamme</div>
			</div>
		</div>
		<div id='div_gamme_upd'></div>
</div>

<? foreach($data_out['liste_gammes'] as $gamme) { ?>
<div id="ID_MENUFond<?=$gamme['id_gamme']?>_ADD_WD" style="
		position:absolute;
		top:50%;
		left:50% ;
		width:600px;
		margin-top:-50px ;
		margin-left:-300px ;
		z-index:11;
		padding:2px 2px 10px;
      border: solid #a5937b 1px;
      background: #fff;
      border-radius: 6px 6px 0 0;
		display: none;">
		<div id="ID_MENUFond<?=$gamme['id_gamme']?>_ADD">	
			<div class="pop_title">
            	<div class="pop_close">
					<a onClick="Disp('Fond<?=$gamme['id_gamme']?>_ADD_WD');"><img
					 src="../imgs/ico_close.png" alt="Fermer" title="Fermer" border="0"></a>
				</div>
				<div id="div_titre_prestation_upd">Ajouter un fond</div>
			</div>
		</div>
		<form name="frm_pastille_add_<?=$gamme['id_gamme']?>" method="post" action="actions.php" autocomplete="off" enctype="multipart/form-data">
      <center>
			<table width="400" class="tab_memo" class="dialog_content" align="center">
				<tr>
				  <td align="left" valign="top"><strong>Marque concern&eacute;e :</strong></td>				
					<td align="left" valign="top">
						<select name="id_marque">
							<option value="0">VDLV</option>
                            <? foreach($data_out['liste_marques'] as $marque) { ?>
                            <option value="<?=$marque['id_marque']?>"><?=$marque['libelle']?></option>
							<? } ?>
						</select>
					</td>
				</tr>
				<tr>
				  <td align="left" valign="top"><strong>Attribut concern&eacute; :</strong></td>
				  <td align="left" valign="top">
					<select name="saveur">
					<? foreach($data_out['liste_saveurs_total'][$gamme['id_gamme']] as $nom_image => $saveur) { ?>
					<option value="<?=$nom_image?>"><?=$nom_image?></option>
					<? } ?>
					</select>
				  </td>
			  </tr>
				<tr>
				  <td align="left" valign="top"><strong>Fond sans nicotine:</strong></td>
				  <td align="left" valign="top"><input name="fond_sans_nicotine" type="file" class="text_input" size="47"></td>
			  </tr>
				<tr>
				  <td align="left" valign="top"><strong>Fond avec nicotine:</strong></td>
				  <td align="left" valign="top"><input name="fond_avec_nicotine" type="file" class="text_input" size="47"></td>
			  </tr>
		  	</table>
		  <br />
			 <input type="submit" name="Submit" value="Ajouter"/>
			 <input type="hidden" name="timestamp" value="<?=time()?>" />
			 <input type="hidden" name="action" value="Pastille_ADD" />
			 <input type="hidden" name="pays" value="" />
			 <input type="hidden" name="id_gamme" value="<?=$gamme['id_gamme']?>" />
			 <input type="hidden" name="show_hide" value="div_gestion_fonds" />			 
        </center>
	  </form>
</div>
<? } ?>

<script type=text/javascript>
	Drag.init(document.getElementById("ID_MENUMarque_ADD"));
	Drag.init(document.getElementById("ID_MENUMarque_UPD"));
	Drag.init(document.getElementById("ID_MENUPays_ADD"));
	Drag.init(document.getElementById("ID_MENUMPays_UPD"));
	<? foreach($data_out['liste_gammes'] as $gamme) { ?>
	Drag.init(document.getElementById("ID_MENUFond<?=$gamme['id_gamme']?>_ADD"));
	<? } ?>

	var deploying_to_prod = false;
	function deploy_to_prod(event) {
		event.preventDefault();
		if (deploying_to_prod) {
			return false;
		}

		deploying_to_prod = true;
		jQuery('#deploy_state').html('| en cours...');
		jQuery.ajax('deploy_dev_to_prod.php', {
			success: function() {
				jQuery('#deploy_state').html('| Fait');
				deploying_to_prod = false;
		    	}
		});
	}

        var copying_to_prod = false;
        function copy_to_prod(event) {
                event.preventDefault();
                if (copying_to_prod) {
                        return false;
                }

                copying_to_prod = true;
                jQuery('#copy_state').html('| en cours... (peux prendre 1 minute)');
                jQuery.ajax('copy_img_dev_to_prod.php', {
                        success: function() {
                                jQuery('#copy_state').html('| Fait');
                                copying_to_prod = false;
                        }
                });
        }

</script>
</html>
